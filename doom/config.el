;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

(add-to-list 'load-path "~/.doom.d/modes/")
(add-to-list 'load-path "~/.doom.d/local/")
;; (add-to-list 'initial-frame-alist '(fullscreen . maximized))

;; This is used until i know how pass works: https://emacs.stackexchange.com/questions/64412/doom-emacs-pass-integration
(require 'protobuf-mode)
(require 'local-config)
(setq default-org-dir "~/org")

;; (org-babel-do-load-languages 'org-babel-load-languages '((csharp . t)))

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name name
      user-mail-address email)

(setq deft-directory default-org-dir)
(setq deft-extensions '("org","md","txt"))
(setq deft-recursive t)
(setq deft-use-filter-string-for-filename t)

(setq org-journal-dir (concat default-org-dir "/journals/"))
(setq org-journal-file-format "%Y-%m-%d.org")
(setq org-journal-date-prefix "#+TITLE: ")
(setq org-journal-date-format "%A, %B %d %Y")

(setq org-roam-directory (concat default-org-dir "/notes/"))

(setq org-agenda-files (directory-files-recursively (concat default-org-dir "/") "\\.org$"))

(after! lsp-ui
(setq lsp-ui-sideline-enable nil)
(setq lsp-ui-imenu-auto-refresh t)
;;(setq lsp-ui-doc-enable t)
(setq lsp-ui-doc-position 'left)
(setq lsp-ui-doc-max-height 80)
(setq lsp-ui-doc-max-width 100)
(setq lsp-headerline-breadcrumb-enable t)
(setq lsp-ui-flycheck-enable t)
(setq lsp-ui-imenu-refresh-delay 10000) ;; i dont know how to use this yet
)

(add-hook 'lsp-on-idle-hook 'lsp-signature-activate)
(setq lsp-signature-render-documentation nil)
(setq lsp-diagnostics-provider :flycheck)
(setq lsp-signature-function 'lsp-signature-posframe)

(setq twittering-icon-mode t)
(setq twittering-use-master-password t)
(setq twittering-initial-timeline-spec-string
      '(":home"))

(map!
 "C-s" #'avy-goto-char-2
 "<f1>" #'lsp-ui-doc-show
 "<f8>" #'next-error
 "<f2>" #'flycheck-list-errors)

(setq +mu4e-backend 'offlineimap)
(setq mu4e-sent-messages-behavior 'delete)


(after! mu4e
(setq sendmail-program email_msmtp_bin_path
      send-mail-function #'smtpmail-send-it
      message-sendmail-f-is-evil t
      message-sendmail-extra-arguments '("--read-envelope-from")
      message-send-mail-function #'message-send-mail-with-sendmail)
  )

(setq mu4e-html2text-command "w3m -T text/html" ; how to handle html-formatted emails
      mu4e-view-show-images t                   ; show images in the view buffer
      mu4e-use-fancy-chars t)                   ; allow fancy icons for mail threads

;; Each path is relative to `+mu4e-mu4e-mail-path', which is ~/.mail by default
;; for now org-msg has an error: https://github.com/hlissner/doom-emacs/issues/4584 so i have disabled org-msg for C-c C-c to work
(setq mu4e-maildir (concat "~/.mail/" email_personal_folder_prefix))

(set-email-account! email_personal_folder_prefix
  `((mu4e-sent-folder       . "/Sent")
    (mu4e-drafts-folder     . "/INBOX")
    (mu4e-trash-folder      . "/Trash")
    (mu4e-refile-folder     . "/Archive")
    (smtpmail-smtp-user     . ,email)
    (user-mail-address      . ,email)    ;; only needed for mu < 1.4
    (mu4e-compose-signature . ,(concat "\nMvh. " name)))
  t)

(after! circe
  (set-irc-server! irc_server
    `(
      :port ,irc_port
      :nick ,irc_nick
      :user ,irc_user
      :pass ,irc_pass
      :channels ())))
;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-vibrant)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory (concat default-org-dir "/"))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
