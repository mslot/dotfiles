;;
;; Testing garbage collection
;;
(setq gc-cons-threshold (* 50 1000 1000))

;;
;; load package manager, add the Melpa package registry
;;
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("MELPA Stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;;
;; bootstrap use-package
;;
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;;
;; better defaults
;;
(use-package better-defaults
  :ensure t)

(load custom-file)

;;
;; user defined functions
;;
(defun eshell-new()
  "Open a new instance of eshell."
  (interactive)
  (eshell 'Z))

(defun open-init-file ()
  "Open the init file."
  (interactive)
  (find-file user-init-file))

(defun switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (switch-to-buffer (other-buffer (current-buffer) 1)))

;;
;; general hooks and startup parameters
;;
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)
(setq inhibit-splash-screen t) 
(setq initial-major-mode (quote fundamental-mode))
(setq frame-resize-pixelwise t)
;;
;; theme, modeline, fonts etc
;;
;; REMEMBER: install fonts: M-x all-the-icons-install-fonts
(add-to-list 'custom-theme-load-path (expand-file-name "~/.emacs.d/themes/"))

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-vibrant t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
  (doom-themes-treemacs-config)
  
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  (setq doom-modeline-indent-info t)
  (setq doom-modeline-enable-word-count t)
  )

;;
;; Nyan cat <3
(use-package nyan-mode
  :defer 1
  :ensure t
  :init
  (nyan-mode))

;;
;; Keybindings
;;
(use-package general
 :ensure t)

 (general-define-key
 :states '(normal)
 :prefix ","
 :non-normal-prefix "C-,"
 :keymaps 'rustic-mode-map

 ;; LSP, and LSP ui + some customizing
 ;; this took longer than expected - i want complete control on what is called, and for some reason which-key integration seems broken on first level, when rebinding 
 "=" '(:ignore t :which-key "formatting")
 "==" '(rustic-format-buffer :which-key "format buffer")
 "F" '(:ignore t :which-key "folders")
 "Fa" '(lsp-workspace-folders-add :which-key "add folder")
 "Fb" '(lsp-workspace-blacklist-remove :which-key "un-blacklist folder")
 "Fr" '(lsp-workspace-folders-remove :which-key "remove folder")
 "G" '(:ignore t :which-key "peek")
 "Gg" '(lsp-ui-peek-find-definitions :which-key "peek definitions")
 "Gi" '(lsp-ui-peek-find-implementation :which-key "peek implementations")
 "Gr" '(lsp-ui-peek-find-references :which-key "peek references")
 "Gs" '(lsp-ui-peek-find-workspace-symbol :which-key "peek workspace symbol")
 "T" '(:ignore t :which-key "toggle")
 "Tl" '(lsp-lens-mode :which-key "toggle lens")
 "TL" '(lsp-toggle-trace-io :which-key "toggle trace io")
 "Th" '(lsp-toggle-symbol-highlight :which-key "toggle highlight")
 "Tb" '(lsp-headerline-breadcrumb-mode :which-key "toggle breadcumb")
 "Ta" '(lsp-modeline-code-actions-mode :which-key "toggle code actions")
 "TD" '(lsp-modeline-diagnostics-mode :which-key "toggle diagnostics")
 "TS" '(lsp-ui-sideline-mode :which-key "toggle sideline")
 "Td" '(lsp-ui-doc-mode :which-key "toggle document popup")
 "Ts" '(lsp-toggle-signature-auto-activate :which-key "toggle signature")
 "Tf" '(lsp-toggle-on-type-formatting :which-key "toggle on type formatting")
 "TT" '(lsp-treemacs-sync-mode :which-key "toggle treemacs sync")
 "a" '(:ignore t :which-key "code actions")
 "aa" '(lsp-execute-code-action :which-key "code actions")
 "al" '(lsp-avy-lens :which-key "avy lens")
 "ah" '(lsp-document-highlight :which-key "highlight symbol")
 "g" '(:ignore t :which-key "goto")
 "gg" '(lsp-find-definition :which-key "find definition")
 "gr" '(lsp-find-references :which-key "find references")
 "gi" '(lsp-find-implementation :which-key "find implementations")
 "gt" '(lsp-find-type-definition :which-key "find type definitions")
 "gd" '(lsp-find-declaration :which-key "find declarations")
 "gh" '(lsp-treemacs-call-hierarchy :which-key "call hierarchy")
 "ga" '(lsp-ivy-workspace-symbol :which-key "find symbol in workspace")
 "gA" '(xref-find-apropos :which-key "list symbols in workspace")
 "ge" '(lsp-treemacs-errors-list :which-key "show errors")
 "h" '(:ignore t :which-key "help")
 "hh" '(lsp-describe-thing-at-point :which-key "describe thing at point")
 "hs" '(lsp-signature-activate :which-key "signature help")
 "hg" '(lsp-ui-doc-glance :which-key "doc glance")
 "r" '(:ignore t :which-key "refactor")
 "rr" '(lsp-rename :which-key "rename")
 "ro" '(lsp-organize-imports :which-key "organize imports")
 "s" '(:ignore t :which-key "sessions")
 "sr" '(lsp-workspace-restart :which-key "restart")
 "ss" '(lsp :which-key "start server")
 "sq" '(lsp-workspace-shutdown :which-key "shutdown server")
 "sd" '(lsp-describe-session :which-key "describe session")
 "sD" '(lsp-disconnect :which-key "disconnect")
 "u" '(:ignore t :which-key "ui")
 "ui" '(lsp-ui-imenu :which-key "imenu")
 "ud" '(lsp-ui-doc-show :which-key "show doc")
 "uD" '(lsp-ui-doc-hide :which-key "hide doc")

 ;; execute
 "x" '(:ignore t :which-key "execute")
 "xr" '(rustic-cargo-run :which-key "run")
 "xt" '(rustic-cargo-test :which-key "test")
 "xx" '(rustic-cargo-build :which-key "build")
 )

(general-define-key
 :keymaps 'override
 :states '(treemacs deft emacs normal)
 :prefix "SPC"
 :non-normal-prefix "C-SPC"
 "" '(:ignore t :which-key "description for SPC")

 ;; General
  "TAB" '(mode-line-other-buffer :which-key "previous window")
  "0" '(treemacs-select-window :which-key "select treemacs")
  "/" '(swiper-isearch :which-key "search")

  ;; shell <3
  "'" '(eshell-new :which-key "shell")
  
 ;; Buffer
 "b" '(:ignore t :which-key "buffers")
 "b d" '(kill-this-buffer :which-key "kill Buffer")
 "b b" '(ivy-switch-buffer :which-key "buffer view")
 "b f" '(avy-goto-char :which-key "goto char")
 "b i" '(counsel-imenu :which-key "imenu")

 ;; Jump
 "j" '(:ignore t :which-key "jump")
 "j j" '(avy-goto-char-timer :which-key "goto char timer")
 "j J" '(avy-goto-char :which-key "goto char")
 "j s" '(swiper :which-key "swiper")

 ;; Applications
 "a" '(:ignore t :which-key "applications")
 "ai" '(znc-all :which-key "IRC")
 "ae" '(:ignore t :which-key "email")
 "aee" '(notmuch :which-key "show")
 "aes" '(counsel-notmuch :which-key "search")

 ;; Frame
 "F" '(:ignore t :which-key "Frame")
 "F n" '(make-frame :which-key "new")
 "F d" '(delete-frame :which-key "delete")

 ;;Debug
 "d" '(:ignore t :which-key "debug")
 "dd" '(dap-debug :which-key "start session")
 "dl" '(dap-debug-last :which-key "last session")
 "db" '(:ignore t :which-key "breakpoints")
 "dba" '(dap-breakpoint-add :which-key "add breakpoint")
 "dbd" '(dap-breakpoint-delete :which-key "delete breakpoint")
 "dbD" '(dap-breakpoint-delete-all :which-key "delete all breakpoints")
 
 ;; Org
 "o" '(:ignore t :which-key "Org")
 "os" '(deft :which-key "search")
 "op" '(org-projectile-capture-for-current-project :which-key "capture for current project")
 "oo" '(org-capture :which-key "capture")
 "oj" '(org-journal-new-entry :which-key "new entry in journal")
 "oc" '(org-projectile-project-todo-completing-read :which-key "capture for project")
 "or" '(org-roam :which-key "roam")
 "oa" '(org-agenda :which-key "agenda")
 
 ;; Project
 "p" '(:ignore t :which-key "project")
 "p t" '(treemacs :which-key "treemacs")
 "p f" '(counsel-projectile-find-file :which-key "find file")
 "p p" '(counsel-projectile :which-key "buffers")
 "p w" '(counsel-projectile-switch-project :which-key "switch project")
 "p T" '(lsp-treemacs-symbols :which-key "symbols")
 "p s" '(:ignore :which-key "search")
 "p s s" '(counsel-projectile-rg :which-key "rg")

 ;; File
 "f" '(:ignore t :which-key "file")
 "ff" '(dired :which-key "find")
 "f e" '(:ignore t :which-key "edit")
 "f e d" '(open-init-file :which-key "init file")

 ;; Window management
 "w" '(:ignore t :which-key "window")
 "w -" '(split-window-vertically :which-key "split vertically")
 "w /" '(split-window-horizontally :which-key "split horizontally")
 "w d" '(delete-window :which-key "delete window")
 "w D" '(kill-buffer-and-window :which-key "kill buffer and window")
 "w w" '(ace-window :which-key "ace window")
 "w m" '(delete-other-windows :which-key "maximaze window")

 ;; Git
 "g" '(:ignore t :which-key "git")
 "g s" '(magit-status :which-key "status")

 ;; Errors
 "e" '(:ignore t :which-key "errors")
 "e e" '(flycheck-list-errors :which-key "list")
 "e n" '(flycheck-next-error :which-key "next error")
 "e p" '(flycheck-previous-error :which-key "previous error")
 )

;;
;; docker-compose
;;
(use-package docker-compose-mode
  :ensure t
  :defer 1)

;;
;; IRC :>
;;
(use-package erc
  :ensure t
  :defer 1
  :custom
  (erc-autojoin-timing 'ident)
  (erc-fill-function 'erc-fill-static)
  (erc-fill-static-center 22)
  (erc-hide-list '("JOIN" "PART" "QUIT"))
  (erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
  (erc-lurker-threshold-time 43200)
  (erc-prompt-for-nickserv-password nil)
  (erc-server-reconnect-attempts 5)
  (erc-server-reconnect-timeout 3)
  (erc-track-exclude-types '("JOIN" "MODE" "NICK" "PART" "QUIT"
                             "324" "329" "332" "333" "353" "477"))
  :config
  (add-to-list 'erc-modules 'notifications)
  (add-to-list 'erc-modules 'spelling)
  (erc-services-mode 1)
  (erc-update-modules))

;; (use-package erc-hl-nicks
  ;; :defer 1
  ;; :ensure t
  ;; :after erc)

(use-package erc-image
  :defer 1
  :ensure t
  :after erc)

(use-package znc
  :defer 1
  :after erc
  :ensure t)

(add-hook 'window-configuration-change-hook 
	   '(lambda ()
	      (setq erc-fill-column (- (window-width) 2))))
;;
;; Dashboard
;;
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

  (setq dashboard-items '((recents  . 5)
                          (projects . 5)
                          ;; (agenda . 5)
                          ))
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-set-init-info t)
  (setq dashboard-startup-banner "~/.emacs.d/emacs-logo.png")
  (setq dashboard-projects-switch-function 'counsel-projectile-switch-project-by-name)
  (setq dashboard-week-agenda t)
;;  (setq dashboard-filter-agenda-entry 'dashboard-no-filter-agenda)

(if (daemonp)
    (progn
     (message "daemon mode detected")
     (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))
  (message "not daemon"))

;;
;; Imenu
;;
(setq imenu-auto-rescan t
      imenu-auto-rescan-maxout (* 1024 1024)
      imenu--rescan-item '("" . -99))

;;
;; Ace
;;
(use-package ace-window
  :defer 1
  :ensure t)
(ace-window-display-mode)

;;
;; Ivy
;;
(use-package ivy-hydra
  :defer 1
  :ensure t
  :after ivy)

(use-package counsel
  :ensure t
  :after ivy
  :config (counsel-mode))

(use-package counsel-projectile
  :ensure t
  :after counsel)

(use-package ivy
  :ensure t
  :defer 1
  :diminish
  :custom
  (ivy-count-format "(%d/%d) ")
  (ivy-use-virtual-buffers t)
  :config (ivy-mode)
  (define-key ivy-minibuffer-map (kbd "C-j") 'next-line)
  (define-key ivy-minibuffer-map (kbd "C-k") 'previous-line)
  (define-key ivy-switch-buffer-map (kbd "C-k") 'previous-line)
  )

(use-package avy
  :defer 1
:ensure t
:config
(avy-setup-default))

(set-face-attribute 'avy-lead-face nil :background "blue" :foreground "white")

(use-package ivy-rich
  :defer 1
  :ensure t
  :after counsel
  :init (setq ivy-rich-path-style 'abbrev
              ivy-virtual-abbreviate 'full)
  :config (ivy-rich-mode))

(use-package swiper
  :defer 1
  :after ivy
  :ensure t)

;;
;; tree sitter
;;
(use-package tree-sitter
  :ensure t)

(use-package tree-sitter-langs
  :after tree-sitter
  :ensure t)

(global-tree-sitter-mode)

;;
;; LSP
;;
(use-package lsp-mode
  :ensure t
  :after tree-sitter
    :hook (
           (csharp-mode . lsp)
           (rustic-mode . (lambda ()
                          (tree-sitter-hl-mode)
                          ))
            (rustic-mode . lsp)
            (lsp-mode . lsp-enable-which-key-integration))
    :commands lsp
    :config
   )

(use-package lsp-pyright
  :defer t
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (require 'dap-python)
                          (lsp))))  ; or lsp-deferred
(lsp-treemacs-sync-mode 1)

(use-package lsp-ui
  :defer 1
  :ensure t
  :commands lsp-ui-mode)

(setq lsp-ui-peek-enable t)
(setq lsp-ui-doc-enable nil)

(use-package lsp-ivy
  :defer 1
  :ensure t
  :commands lsp-ivy-workspace-symbol)

(use-package lsp-treemacs
  :defer 1
  :ensure t
  :commands lsp-treemacs-errors-list)

;;
;; DAP
;;
(use-package dap-mode
  :defer 1
  :after lsp
  :ensure t)

(require 'dap-gdb-lldb)
(dap-register-debug-template "Rust::GDB Run Configuration"
                             (list :type "gdb"
                                   :request "launch"
                                   :name "GDB::Run"
				   :gdbpath "rust-gdb"
                                   :target nil
                                   :cwd nil))
(add-hook 'dap-stopped-hook
          (lambda (arg) (call-interactively #'dap-hydra)))

;;
;; which key
;;
(use-package which-key
  :ensure t
    :config
    (which-key-mode))
(setq lsp-enable-snippet nil)

;;
;; flycheck
;;
(use-package flycheck
  :defer 1
  :ensure t
  :init (global-flycheck-mode))

;;
;; company mode
;;
(use-package company
  :defer 1
  :ensure t
  :config

  (define-key company-active-map (kbd "C-j") 'company-select-next-or-abort)
  (define-key company-active-map (kbd "C-k") 'company-select-previous-or-abort)
  (define-key company-active-map (kbd "C-l") 'company-complete-selection)
  (define-key company-active-map (kbd "C-h") 'company-abort)
  (define-key company-active-map (kbd "<C-return>") 'company-complete-selection)
  )

(add-hook 'after-init-hook 'global-company-mode)

(use-package company-box
  :defer 1
  :ensure t
  :hook (company-mode . company-box-mode))

;;
;; rustic
;;
(use-package rust-mode
  :ensure t
  :defer 1)

(use-package rustic
  :ensure t
  :after rust-mode
  :defer 1)

(add-hook 'rustic-mode-hook
	  (lambda ()

            (add-hook 'before-save-hook (lambda()
                                          (lsp-format-buffer)))
	    (setq ident-tabs-mode nil)
            (setq lsp-rust-analyzer-cargo-watch-command "clippy")
            (setq rustic-format-on-save nil)
	    ))
;;
;; LISP
;;
(add-hook 'emacs-lisp-mode-hook
  (lambda ()
    (company-mode)
    (add-hook 'before-save-hook
              (lambda ()
                (message "saved lisp buffer")
                )
    ))
  )

;;
;; load evil
;;
(use-package undo-tree
  :ensure t
  :defer 1
  :after evil)

(use-package evil
  :after which-key
  :ensure t ;; install the evil package if not installed
  :init ;; tweak evil's configuration before loading it
  (setq evil-want-keybinding nil)
  (setq evil-search-module 'evil-search)
  (setq evil-ex-complete-emacs-commands nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-shift-round nil)
  (setq evil-want-C-u-scroll t)
  :config ;; tweak evil after loading it
  (progn
    (evil-mode)
    (global-undo-tree-mode)
    (evil-set-undo-system 'undo-tree)
    (define-key evil-normal-state-map (kbd "C-h") 'evil-window-left)
    (define-key evil-normal-state-map (kbd "C-j") 'evil-window-down)
    (define-key evil-normal-state-map (kbd "C-k") 'evil-window-up)
    (define-key evil-normal-state-map (kbd "C-l") 'evil-window-right)
    ))

;;
;; evil-collection
;;
(use-package evil-collection
  :after evil
  :defer 1
  :ensure t
  :config
  (evil-collection-init))

;;
;; load treemacs
;;
(use-package treemacs
  :ensure t
  :defer 1
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay      0.5
          treemacs-directory-name-transformer    #'identity
          treemacs-display-in-side-window        t
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              5000
          treemacs-file-extension-regex          treemacs-last-period-regex-value
          treemacs-file-follow-delay             0.2
          treemacs-file-name-transformer         #'identity
          treemacs-follow-after-init             t
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-move-forward-on-expand        nil
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                      'left
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    nil
          treemacs-recenter-after-tag-follow     nil
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'on-distance
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              nil
          treemacs-silent-refresh                nil
          treemacs-sorting                       'alphabetic-asc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-user-mode-line-format         nil
          treemacs-user-header-line-format       nil
          treemacs-width                         35
          treemacs-workspace-switch-cleanup      nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (setq treemacs-position 'right)
    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-evil
  :defer 1
  :after treemacs evil
  :ensure t)

(use-package projectile
  :defer 1
  :ensure t)
(projectile-mode +1)

(use-package treemacs-projectile
  :defer 1
  :after treemacs projectile
  :ensure t)

(use-package treemacs-icons-dired
  :defer 1
  :after treemacs dired
  :ensure t
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :defer 1
  :after treemacs magit
  :ensure t)

;;
;; Org-mode
;; Org-roam
;; Deft
;;
(use-package org-bullets
  :defer 1
  :ensure t)

(add-hook 'org-mode-hook 'org-indent-mode)

(setq org-startup-folded "showall")
(add-hook 'org-mode-hook (lambda ()
   "Beautify Org Checkbox Symbol"
   (push '("[ ]" .  "☐") prettify-symbols-alist)
   (push '("[X]" . "☑" ) prettify-symbols-alist)
   (push '("[-]" . "❍" ) prettify-symbols-alist)
   (prettify-symbols-mode)
   (setq org-log-done 'time)
   (org-bullets-mode 1)))

(use-package deft
  :defer 1
  :ensure t)

(use-package org-roam
  :defer 1
  :ensure t)
(setq default-org-dir "~/org")

;; deft
(setq deft-directory default-org-dir)
(setq deft-extensions '("org","md","txt"))
(setq deft-recursive t)
(setq deft-use-filter-string-for-filename t)

;; org base
(setq org-agenda-files nil)
(setq org-tags-column 0)

;; org journal - configured to match org-roam recommended config https://www.orgroam.com/manual/Org_002djournal.html
(setq org-journal-dir (concat default-org-dir "/journals/"))
(setq org-journal-file-format "%Y-%m-%d.org")
(setq org-journal-date-prefix "#+TITLE: ")
(setq org-journal-date-format "%A, %B %d %Y")

(use-package org-journal
  :defer 1
  :ensure t)

;; org-roam
(setq org-roam-directory (concat default-org-dir "/notes/"))

;; org mode
(setq org-agenda-files (directory-files-recursively (concat default-org-dir "/") "\\.org$"))

(setq org-capture-templates
      `(
        ("t" "General TODOs" entry (file ,(concat default-org-dir "/todos/todo.org"))
         "** TODO %?")
        ))

;; org projectile
(use-package org-projectile
  :defer 1
  :ensure t)

(org-projectile-per-project)
(setq org-projectile-capture-template "* TODO %?\n")

(with-eval-after-load 'org-agenda
  (require 'org-projectile)
  (mapcar '(lambda (file)
             (when (file-exists-p file)
               (push file org-agenda-files)))
          (org-projectile-todo-files)))

(use-package treemacs-persp ;;treemacs-persective if you use perspective.el vs. persp-mode
  :defer 1
  :after treemacs persp-mode ;;or perspective vs. persp-mode
  :ensure t
  :config (treemacs-set-scope-type 'Perspectives))

;;
;; notmuch
;;
(use-package notmuch
  :defer 1
  :after evil
  :ensure t)

(use-package counsel-notmuch
  :defer 1
  :after notmuch
  :ensure t)

;;
;; eshell
;;

(setq eshell-prompt-regexp "^[^#$\n]*[\n#$] "
      eshell-prompt-function
      (lambda nil
        (concat
	 "[" (user-login-name) "@" (system-name) " "
	 (if (string= (eshell/pwd) (getenv "HOME"))
	     "~" (eshell/basename (eshell/pwd)))
	 "]"
	 (if (= (user-uid) 0) "\n# " "\n$ "))))

;;
;; Test garbage collection
;;

(setq gc-cons-threshold (* 10 1000 1000))
